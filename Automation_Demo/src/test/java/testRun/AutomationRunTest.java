package testRun;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomationRunTest {

	WebDriver driver;

	@Before
	public void BrowserRun() {

		System.setProperty("webdriver.chrome.driver", "driver\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.get("https://itera-qa.azurewebsites.net/home/automation");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	@Test
	public void testCaseRun() {

		WebElement name = driver.findElement(By.xpath("//input[@id='name']"));
		WebElement mobile_Number = driver.findElement(By.xpath("//input[@id='phone']"));
		WebElement email_Address = driver.findElement(By.xpath("//input[@id='email']"));
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		WebElement address = driver.findElement(By.xpath("//textarea[@id='address']"));
		WebElement submit_Button = driver.findElement(By.xpath("//button[@name='submit']"));

		name.sendKeys("Selenium");
		mobile_Number.sendKeys("646-500-0720");
		email_Address.sendKeys("mdaripon87@gmail.com");
		password.sendKeys("abc123");
		address.sendKeys("150th street");
		submit_Button.click();

		WebElement gender = driver.findElement(By.id("male"));
		WebElement day = driver.findElement(By.id("monday"));
		WebElement travel_Year = driver.findElement(By.xpath("//select[@class='custom-select']"));
		WebElement test_Automation = driver.findElement(By.xpath("//label[text()='1 year']"));
		WebElement automation_Tool = driver.findElement(By.xpath("//label[text()='Selenium Webdriver']"));

		gender.click();
		day.click();

		Select sc = new Select(travel_Year);
		sc.selectByVisibleText("Denmark");

		List<WebElement> options = sc.getOptions();
		for (WebElement i : options) {
			System.out.println(i.getText());

		}

		test_Automation.click();
		automation_Tool.click();

	}

	@After
	public void tearDown() {
		driver.close();
		driver.quit();
	}

}
